#ifndef NODE_H
#define NODE_H

#include <QPoint>
#include <QString>

class Node
{
  public:
    Node();


    QPoint getPosition() const;
    void setPosition(const QPoint &value);
    QPoint getLabelPosition() const;
    void setLabelPosition(const QPoint &value);

    QPoint position, labelPosition;
    QString label;
    double height, width;

};

#endif // NODE_H
