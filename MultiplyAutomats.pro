#-------------------------------------------------
#
# Project created by QtCreator 2015-05-03T23:47:25
#
#-------------------------------------------------

QT       += core gui
LIBS += -lgvc -lcgraph -lcdt
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MultiplyAutomats
TEMPLATE = app


SOURCES += main.cpp\
        automatview.cpp \
    graph.cpp \
    state.cpp \
    transition.cpp \
    path.cpp \
    node.cpp \
    automat.cpp \
    combinedtransition.cpp \
    pathinput.cpp \
    simplegui.cpp

HEADERS  += automatview.h \
    graph.h \
    state.h \
    transition.h \
    path.h \
    node.h \
    automat.h \
    combinedtransition.h \
    pathinput.h \
    simplegui.h
