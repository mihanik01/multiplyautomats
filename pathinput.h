#ifndef PATHINPUT_H
#define PATHINPUT_H

#include <QWidget>
#include <QCheckBox>
#include <QLabel>
#include <QComboBox>
#include <QLayout>

#include "automat.h"
#include "automatview.h"

class PathInput : public QWidget
{
    Q_OBJECT
  public:
    explicit PathInput(
        QString nodeName,
        QString signalName,
        unsigned nodeIndexNum,
        unsigned outNumber,
        QWidget *parent = 0);

    void deactivate();
    void activate();

    void setNodeNum(unsigned n);
    void setOutNum(unsigned n);

    bool getUsePath();
    unsigned getEndNodeIndex();
    unsigned getOutIndex();
    QString getInSignal();
    QString getStartNode();

  signals:
    void change();
  public slots:
    void catchChange();
  private:
    QString nodeName;
    QString signalName;

    unsigned nodeIndexNum;
    unsigned outNumber;

    bool isActive;

    QCheckBox *usePath;
    QLabel *helper, *toLabel, *outLabel;
    QComboBox *nodeSelector, *outSelector;
    QLayout *mainLayout;
};

#endif // PATHINPUT_H
