#include "state.h"
#include "transition.h"
#include "combinedtransition.h"

State::State(QString _name):
  name(_name)
{}

void State::addTransition(QString signal, QString output, State *dest)
{
  transitions.append(new Transition(signal, output, dest));
}

Transition* State::addCombined(QString signal, QString out1, QString out2, State *dest)
{
  Transition* trans = new CombinedTransition(signal, out1, out2, dest);
  transitions.append(trans);
  return trans;
}


QList<Transition*> State::getTransitions() const
{
  return transitions;
}

QString State::getName() const
{
  return name;
}

bool State::equals(State &st) const {
  return this->name == st.name;
}

bool State::operator==(State &st) const {
  return this->equals(st);
}
