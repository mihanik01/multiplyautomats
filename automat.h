#ifndef AUTOMAT_H
#define AUTOMAT_H

#include "state.h"
#include <QMap>
#include <QObject>
#include <QString>
#include "graph.h"

class Automat: public QObject
{
    Q_OBJECT
  public:
    Automat();
    Graph* getGraph();

    void addState(QString name);
    void removeState(QString name);
    void clear();

    void addRoute(QString src, QString dst, QString in, QString out);
    void addCombined(QString src, QString dst, QString signal, QString out1, QString out2);

    bool hasState(QString name);

    Automat* multiply(Automat &a2);
    QMap<QString, State> getStates() const;

    void startChange();
    void endChange();

  signals:
    void change();
  private:
    QMap<QString, State> states;
    Graph g;
    bool inChange = false;

    void triggerChange();
};

#endif // AUTOMAT_H
