#include "pathinput.h"


PathInput::PathInput(QString nodeName,
                     QString signalName,
                     unsigned nodeIndexNum,
                     unsigned outNumber,
                     QWidget *parent):
  nodeName(nodeName), signalName(signalName),
  QWidget(parent)
{
  mainLayout = new QHBoxLayout(this);

  usePath = new QCheckBox(this);
  helper = new QLabel(QString("Node: %1, input signal: %2.").arg(nodeName, signalName),
                      this);
  toLabel = new QLabel("State to: ", this);
  outLabel = new QLabel("Emit signal: ", this);
  nodeSelector = new QComboBox(this);
  outSelector = new QComboBox(this);

  setNodeNum(nodeIndexNum);
  setOutNum(outNumber);

  mainLayout->addWidget(usePath);
  mainLayout->addWidget(helper);
  mainLayout->addWidget(toLabel);
  mainLayout->addWidget(nodeSelector);
  mainLayout->addWidget(outLabel);
  mainLayout->addWidget(outSelector);

  connect(usePath, SIGNAL(clicked(bool)), this, SLOT(catchChange()));
  connect(nodeSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(catchChange()));
  connect(outSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(catchChange()));
}

void PathInput::deactivate()
{
 isActive = false;
 usePath->setEnabled(false);
 nodeSelector->setEnabled(false);
 outSelector->setEnabled(false);
}

void PathInput::activate()
{
 isActive = true;
 usePath->setEnabled(true);
 nodeSelector->setEnabled(true);
 outSelector->setEnabled(true);
}

void PathInput::setNodeNum(unsigned n)
{
  nodeIndexNum = n;
  nodeSelector->clear();
  for(unsigned i = 0; i < n; i++)
    nodeSelector->addItem(QString::number(i));
}

void PathInput::setOutNum(unsigned n)
{
  outNumber = n;
  outSelector->clear();
  for(unsigned i = 0; i < n; i++)
    outSelector->addItem(QString::number(i));
}

bool PathInput::getUsePath()
{
  return usePath->isChecked();
}

unsigned PathInput::getEndNodeIndex()
{
  return nodeSelector->currentIndex();
}

unsigned PathInput::getOutIndex()
{
  return outSelector->currentIndex();
}

QString PathInput::getInSignal()
{
  return signalName;
}

QString PathInput::getStartNode()
{
  return nodeName;
}

void PathInput::catchChange()
{
  emit change();
}
