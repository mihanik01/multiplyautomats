#include "automatview.h"

#include "graph.h"
#include <QGraphicsView>
#include <QGraphicsEllipseItem>

AutomatView::AutomatView(Automat *a, QWidget *parent)
    : QWidget(parent), automat(a)
{
  graphics = new QGraphicsScene(this);
  view = new QGraphicsView(graphics, this);
  connect(automat, SIGNAL(change()), this, SLOT(drawGraph()));
  drawGraph();
}

void AutomatView::drawGraph()
{
  QFont font;
  font.setPixelSize(20);
  font.setFamily("Arial");
  Graph* g = automat->getGraph();
  graphics->clear();

  QList<Node> nodes = g->nodes();
  QRectF rect = g->boundingRect();
  for (const Node& node : nodes) {
    graphics->addEllipse(node.position.x()-node.width/2,
                         rect.height()-(node.position.y()+node.height/2),
                         node.width,
                         node.height);
    QGraphicsTextItem *txt = new QGraphicsTextItem;
    txt->setFont(font);
    txt->setPos(node.getLabelPosition().x()-node.width/2,
                rect.height()-node.getLabelPosition().y()-node.height/2+font.pixelSize()/2);
    txt->setPlainText(node.label);
    graphics->addItem(txt);
  }

  for (const Path &path : g->edges()) {
    QGraphicsPathItem *pathItem = new QGraphicsPathItem();

    pathItem->setPath(path.points);

    graphics->addItem(pathItem);
    QGraphicsTextItem *txt = new QGraphicsTextItem;
    txt->setFont(font);
    txt->setPos(path.getLabelPos().x()-font.pixelSize()*1.6,
                rect.height()-path.getLabelPos().y()-font.pixelSize());
    txt->setPlainText(path.getLabel());
    graphics->addItem(txt);
  }

  view->setSceneRect(rect);
  view->resize(rect.width()+100, rect.height()+100);
  this->setMinimumSize(view->size());
  view->show();
}

AutomatView::~AutomatView() {
}

void AutomatView::setAutomat(Automat *a)
{
  disconnect(automat, SIGNAL(change()), this, SLOT(drawGraph()));
  automat = a;
  connect(automat, SIGNAL(change()), this, SLOT(drawGraph()));
  drawGraph();
}
