#include "automatview.h"
#include "simplegui.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SimpleGUI pi(5, 2, 2);
    pi.show();

    return a.exec();
}
