#ifndef GRAPH_H
#define GRAPH_H

#define HAVE_STRING_H

#include <graphviz/gvc.h>
#include <QMap>
#include <QList>
#include <QString>
#include <QRect>

#include "path.h"
#include "node.h"

class Graph
{
    public:
    Graph(QString name);
    ~Graph();

    // Добавляем узел графа и назначаем ему метку name
    void addNode(const QString& name);
    // Удаляем узел с именем name
    void removeNode(const QString& name);
    // Удаляем все узлы из графа
    void clearNodes();

    // Добавляем стрелочку от source к target
    void addEdge(const QString& source, const QString& target, const QString &name);
    // Удаляем стрелочку
    void removeEdge(const QString& source, const QString& target, const QString &name);
    void removeEdge(const QPair<QPair<QString, QString>, QString>& key);

    // Это надо вызвать после того как добавили все элементы,
    // перед отрисовкой стредствами Qt или любыми другими средствами
    void applyLayout();
    void free();
    QRectF boundingRect() const;


    // Получить список путей в виде объектов Path
    QList<Path> edges() const;
    // Получить список вершин в виде объектов Node
    QList<Node> nodes() const;

    // Это вспомогательная функция
    QPolygon createTriangle(qreal r, qreal phi, qreal x, qreal y) const;
  private:
    const qreal node_size = 50;
    const qreal DotDefaultDPI = 72.0;

    GVC_t *_context;
    Agraph_t *_graph;
    QMap<QString, Agnode_t*> _nodes;
    QMap<QPair<QPair<QString, QString>, QString>, Agedge_t*> _edges;

    static inline Agraph_t* _agopen(QString name, Agdesc_t& kind);
    static inline QString _agget(void *object, QString attr, QString alt=QString());
    static inline int _agset(void *object, QString attr, QString value);
    static inline Agsym_t* _agattr(Agraph_t * graph, int kind, QString name, QString value);
};

#endif // GRAPH_H
