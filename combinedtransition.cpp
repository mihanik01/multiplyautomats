#include "combinedtransition.h"

CombinedTransition::CombinedTransition(QString in, QString out1, QString out2, State *dest):
  Transition(in, out1, dest), out2(out2)
{}

QString CombinedTransition::getOutput() const
{
  if (output == out2)
    return output;
  else
    return "("+output+", "+out2+")";
}


