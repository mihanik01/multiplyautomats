#ifndef COMBINEDTRANSITION_H
#define COMBINEDTRANSITION_H

#include "transition.h"


class CombinedTransition : public Transition
{
  public:
    CombinedTransition(QString in, QString out1, QString out2, State *dest);
    QString getOutput() const;
  private:
    QString out2;
};

#endif // COMBINEDTRANSITION_H
