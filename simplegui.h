#ifndef SIMPLEGUI_H
#define SIMPLEGUI_H

#include <QWidget>

#include "automatview.h"
#include "automat.h"
#include <QLayout>
#include <QComboBox>
#include <QVector>
#include "pathinput.h"

class SimpleGUI : public QWidget
{
    Q_OBJECT
  public:
    explicit SimpleGUI(unsigned nodeNum,
                       unsigned outNum,
                       unsigned inNum,
                       QWidget *parent = 0);

  signals:

  public slots:
    void automat1Change();
    void automat2Change();
    void node1NumChange(int newNumIndex);
    void node2NumChange(int newNumIndex);
    void nodeNumChange(int newNumIndex, int autindex);
  private:
    unsigned nodeNum, outNum, inNum;
    Automat *automat[2], *automat3;
    QString a1Prefix, a2Prefix, a3Prefix;

    QVector<PathInput*> pathInputs[2];
    QComboBox *node1NumCombo, *node2NumCombo;
    AutomatView *autView1, *autView2, *autViewResult;

    void nodeNumComboSetter(QComboBox *nodeNumCombo, QString prefix);
    void setupAutomat(unsigned index, unsigned nodeNum);
    void automatChange(unsigned index);
};

#endif // SIMPLEGUI_H
