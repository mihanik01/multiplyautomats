#ifndef STATE_H
#define STATE_H

#include <QString>
#include <QList>
class Transition;

class State
{
  public:
    State(QString _name);
    void addTransition(QString signal, QString output, State *dest);
    Transition* addCombined(QString signal1, QString out1, QString out2, State *dest);

    QList<Transition*> getTransitions() const;
    QString getName() const;

    bool equals(State &st) const;
    bool operator==(State &st) const;

  private:
    QString name;
    QList<Transition*> transitions;
};


#endif // STATE_H
