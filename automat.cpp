#include "automat.h"

#include <QSet>

Automat::Automat():g("Graph")
{}

Graph *Automat::getGraph()
{
  g.applyLayout();
  return &g;
}

void Automat::addState(QString name)
{
  if (!hasState(name)) {
    states.insert(name, State(name));
    g.free();
    g.addNode(name);
    triggerChange();
  }
}

void Automat::removeState(QString name)
{
  if (hasState(name)) {
    g.free();
    g.removeNode(name);
    triggerChange();
  }
}

void Automat::clear()
{
  g.free();
  g.clearNodes();
  states.clear();
}

void Automat::addRoute(QString src, QString dst,
                       QString in, QString out)
{
  if (hasState(src) && hasState(dst)) {
    State *from = &states.find(src).value();
    State *to   = &states.find(dst).value();

    from->addTransition(in, out, to);
    g.free();
    g.addEdge(src, dst, in+"/"+out);
    triggerChange();
  }
}

void Automat::addCombined(QString src, QString dst,
                          QString signal, QString out1,
                          QString out2)
{
  if (hasState(src) && hasState(dst)) {
    State *from = &states.find(src).value();
    State *to   = &states.find(dst).value();

    Transition *tr = from->addCombined(signal, out1, out2, to);
    g.free();
    g.addEdge(src, dst, tr->getLabel());
    triggerChange();
  }
}

bool Automat::hasState(QString name)
{
  return states.contains(name);
}

Automat* Automat::multiply(Automat &a2)
{
  Automat* result = new Automat();
  for(const State &s1 : states) {
    for (const State &s2 : a2.getStates()) {
      result->addState(s1.getName()+", "+s2.getName());
    }
  }

  for(const State &s1 : states) {
    QSet<Transition*> notfounds, removals;
    for (const State &s2 : a2.getStates()) {
      notfounds.clear();
      for(Transition *tr1 : s1.getTransitions()) {
        QString signal = tr1->getInput();
        Transition *a2tr = nullptr;

        for(Transition *tr2 : s2.getTransitions()) { 
          if (tr2->getInput() == signal) {
            a2tr = tr2;
            removals.insert(tr2);
          } else {
            notfounds.insert(tr2);
          }
        }
        if (a2tr) {
          result->addCombined(s1.getName()+", "+s2.getName(),
                             tr1->getDestination()->getName()+", "+a2tr->getDestination()->getName(),
                             signal, tr1->getOutput(), a2tr->getOutput());
        } else {
          result->addCombined(s1.getName()+", "+s2.getName(),
                             tr1->getDestination()->getName()+", "+s2.getName(),
                             signal, tr1->getOutput(), "");
        }
      }
      notfounds -= removals;
      for (Transition *tr2 : notfounds) {
        result->addCombined(s1.getName()+", "+s2.getName(),
                           s1.getName()+", "+tr2->getDestination()->getName(),
                           tr2->getInput(), "", tr2->getOutput());
      }
    }
  }
  return result;
}

QMap<QString, State> Automat::getStates() const
{
  return states;
}

void Automat::startChange()
{
  inChange = true;
}

void Automat::endChange()
{
  inChange = false;
  triggerChange();
}


void Automat::triggerChange()
{
  if (!inChange)
    emit change();
}

