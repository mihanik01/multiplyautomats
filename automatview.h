#ifndef AUTOMATVIEW_H
#define AUTOMATVIEW_H

#include <QWidget>

#include <QGraphicsScene>
#include "automat.h"

class AutomatView : public QWidget
{
    Q_OBJECT

  public:
    AutomatView(Automat *a, QWidget *parent = 0);
    ~AutomatView();

    void setAutomat(Automat *a);

  public slots:
    void drawGraph();

  private:
    QGraphicsScene *graphics;
    QGraphicsView *view;
    Automat* automat;
};

#endif // AUTOMATVIEW_H
