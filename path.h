#ifndef PATH_H
#define PATH_H

#include "transition.h"
#include <QPainterPath>

class Path
{
  public:
    Path();

    QPainterPath getPoints() const;
    void setPoints(const QPainterPath &value);

    QString getLabel() const;
    void setLabel(const QString &value);

    QPoint getLabelPos() const;
    void setLabelPos(const QPoint &value);

  //private:
    QString label;
    QPainterPath points;
    QPoint labelPos;
};

#endif // PATH_H
