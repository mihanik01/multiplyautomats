#include "simplegui.h"

#include <QScrollArea>

SimpleGUI::SimpleGUI(unsigned nodeNum,
                     unsigned outNum,
                     unsigned inNum,
                     QWidget *parent)
  :QWidget(parent), nodeNum(nodeNum), outNum(outNum), inNum(inNum)
{
  a1Prefix = "a";
  a2Prefix = "b";
  a3Prefix = "c";

  automat[0] = new Automat();
  automat[1] = new Automat();
  automat3 = new Automat();
  QLayout *mainLayout = new QVBoxLayout(this),
          *setupLayout = new QHBoxLayout(),
          *modifAut1Layout = new QVBoxLayout(),
          *modifAut2Layout = new QVBoxLayout();

  QScrollArea *scrollArea = new QScrollArea(this);
  scrollArea->setFixedHeight(350);
  mainLayout->addWidget(scrollArea);
    QWidget *contents = new QWidget;
    QLayout *showAutLayout = new QHBoxLayout(contents);
    autView1 = new AutomatView(automat[0], contents);
    autView2 = new AutomatView(automat[1], contents);
    autViewResult = new AutomatView(automat3, contents);
    showAutLayout->setSizeConstraint(QLayout::SetMinimumSize);
    scrollArea->setWidget(contents);
  mainLayout->addItem(setupLayout);
  setupLayout->addItem(modifAut1Layout);
  setupLayout->addItem(modifAut2Layout);


  showAutLayout->addWidget(autView1);
  showAutLayout->addWidget(autView2);
  showAutLayout->addWidget(autViewResult);
  autView1->show();

  node1NumCombo = new QComboBox(this);
  nodeNumComboSetter(node1NumCombo, a1Prefix);
  node2NumCombo = new QComboBox(this);
  nodeNumComboSetter(node2NumCombo, a2Prefix);
  connect(node1NumCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(node1NumChange(int)));
  connect(node2NumCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(node2NumChange(int)));

  modifAut1Layout->addWidget(node1NumCombo);
  modifAut2Layout->addWidget(node2NumCombo);

  for (unsigned i = 0; i < nodeNum; i++) {
    for (unsigned j = 0; j < inNum; j++) {
      pathInputs[0].append(new PathInput(a1Prefix+QString::number(i),
                                       "s"+QString::number(j),
                                       node1NumCombo->currentIndex()+1,
                                       outNum,
                                       this));
      pathInputs[1].append(new PathInput(a2Prefix+QString::number(i),
                                       "s"+QString::number(j),
                                       node2NumCombo->currentIndex()+1,
                                       outNum,
                                       this));
      if (i <= node1NumCombo->currentIndex())
        pathInputs[0].last()->activate();
      else
        pathInputs[0].last()->deactivate();

      if (i <= node2NumCombo->currentIndex())
        pathInputs[1].last()->activate();
      else
        pathInputs[1].last()->deactivate();
      modifAut1Layout->addWidget(pathInputs[0].last());
      modifAut2Layout->addWidget(pathInputs[1].last());

      connect(pathInputs[0].last(), SIGNAL(change()), this, SLOT(automat1Change()));
      connect(pathInputs[1].last(), SIGNAL(change()), this, SLOT(automat2Change()));
    }
  }
  node1NumChange(node1NumCombo->currentIndex());
  node2NumChange(node2NumCombo->currentIndex());
}

void SimpleGUI::node1NumChange(int newNumIndex) {
  nodeNumChange(newNumIndex, 0);
  automat1Change();
}

void SimpleGUI::node2NumChange(int newNumIndex) {
  nodeNumChange(newNumIndex, 1);
  automat2Change();
}

void SimpleGUI::nodeNumChange(int newNumIndex, int autindex) {
  for (unsigned i = 0; i < nodeNum; i++) {
    for (unsigned j = 0; j < inNum; j++) {
      if (i <= newNumIndex) {
        pathInputs[autindex][i*inNum+j]->activate();
      } else {
        pathInputs[autindex][i*inNum+j]->deactivate();
      }
      pathInputs[autindex][i*inNum+j]->setNodeNum(newNumIndex+1);
    }
  }
}

void SimpleGUI::nodeNumComboSetter(QComboBox *nodeNumCombo, QString prefix) {
  QString accumulator = prefix;
  for (unsigned i = 1; i <= nodeNum; i++) {
    nodeNumCombo->addItem(accumulator += QString::number(i-1));
    accumulator += ", " + prefix;
  }
}

void SimpleGUI::automat1Change() {
  automatChange(0);
}

void SimpleGUI::automat2Change() {
  automatChange(1);
}

void SimpleGUI::setupAutomat(unsigned index, unsigned nodeNum) {
  automat[index]->clear();
  automat[index]->startChange();
  QString prefix = (index == 0) ? a1Prefix : a2Prefix;

  for(unsigned i = 0; i < nodeNum; i++) {
    automat[index]->addState(prefix + QString::number(i));
  }

  for(unsigned i = 0; i < nodeNum*inNum; i++) {
    if (pathInputs[index][i]->getUsePath()) {
      automat[index]->addRoute(pathInputs[index][i]->getStartNode(),
                               prefix+QString::number(pathInputs[index][i]->getEndNodeIndex()),
                               pathInputs[index][i]->getInSignal(),
                               prefix+QString::number(pathInputs[index][i]->getOutIndex()));
    }
  }

  automat[index]->endChange();
}

void SimpleGUI::automatChange(unsigned index)
{
  QComboBox *combo = (index == 0) ? node1NumCombo : node2NumCombo;
  setupAutomat(index, combo->currentIndex()+1);
  Automat *res = automat[0]->multiply(*automat[1]);
  autViewResult->setAutomat(res);
  delete automat3;
  automat3 = res;
  this->adjustSize();
}
