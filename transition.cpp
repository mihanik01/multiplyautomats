#include "transition.h"

Transition::Transition(QString in, QString out, State *dest)
  :input(in), output(out), destination(dest)
{}

QString Transition::getLabel() const {
  QString result;
  if (!input.isEmpty())
    result += input;
  if (!input.isEmpty() && !getOutput().isEmpty())
    result += "/";
  if (!getOutput().isEmpty())
    result += getOutput();
  return result;
}
State *Transition::getDestination() const
{
    return destination;
}
QString Transition::getOutput() const
{
    return output;
}
QString Transition::getInput() const
{
    return input;
}



