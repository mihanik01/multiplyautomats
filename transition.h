#ifndef TRANSITION_H
#define TRANSITION_H

#include <QString>
#include "state.h"

class Transition
{
  public:
    Transition(QString in, QString out, State *dest);

    QString getLabel() const;
    State *getDestination() const;
    virtual QString getOutput() const;
    QString getInput() const;

  protected:
    QString input, output;
    State *destination;
};

#endif // TRANSITION_H
