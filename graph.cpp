#include "graph.h"
#include <graphviz/cgraph.h>
#include <graphviz/types.h>

#include <math.h>

Graph::Graph(QString name) :
  _context(gvContext()),
  _graph(_agopen(name, Agdirected))
{
  _agset(_graph, "overlap", "prism");
  _agset(_graph, "splines", "true");
  _agset(_graph, "pad", "0.2");
  _agset(_graph, "dpi", "96.0");
  _agset(_graph, "nodesep", "0.4");

  _agattr(_graph, AGNODE, "regular", "true");
  _agattr(_graph, AGNODE, "fixedsize", "true");
  _agattr(_graph, AGNODE, "label", "");

  QString nodePtsWidth = QString("%1").arg(node_size/_agget(_graph, "dpi", "96.0").toDouble());
  _agattr(_graph, AGNODE, "width", nodePtsWidth);

  _agset(_graph, "fontname", "Arial");
  _agset(_graph, "fontsize", "14");

  _agattr(_graph, AGNODE, "fontname", "Arial");
  _agattr(_graph, AGNODE, "fontsize", "14");

  _agattr(_graph, AGEDGE, "fontname","Arial");
  _agattr(_graph, AGEDGE, "fontsize", "14");
}

Graph::~Graph() {
  gvFreeLayout(_context, _graph);
  agclose(_graph);
  gvFreeContext(_context);
}

void Graph::addNode(const QString& name)
{
    if(_nodes.contains(name))
        removeNode(name);
    Agnode_t *node = agnode(_graph, const_cast<char*>(qPrintable(name)), TRUE);
    _agset(node, "label", name);
    _nodes.insert(name, node);
}

void Graph::removeNode(const QString& name)
{
    if(_nodes.contains(name))
    {
        agdelete(_graph, _nodes[name]);
        _nodes.remove(name);

        QList<QPair<QPair<QString, QString>, QString> >keys=_edges.keys();
        for(int i=0; i<keys.size(); ++i)
            if(keys.at(i).first.first==name || keys.at(i).first.second==name)
                removeEdge(keys.at(i));
    }
}

void Graph::clearNodes()
{
    QList<QString> keys=_nodes.keys();

    for(int i=0; i<keys.size(); ++i)
        removeNode(keys.at(i));
}

void Graph::addEdge(const QString &source,
                    const QString &target,
                    const QString &name)
{
  if(_nodes.contains(source) && _nodes.contains(target)) {
    QPair<QPair<QString, QString>, QString> key(QPair<QString, QString>(source, target), name);
    if(!_edges.contains(key)) {
      Agedge_t* edge = agedge(_graph, _nodes[source],
                              _nodes[target],
                              const_cast<char*>(qPrintable(name)),
                              TRUE);
      _agset(edge, "label", name);
      _edges.insert(key, edge);
    }
  }
}

void Graph::removeEdge(const QString &source, const QString &target, const QString &name)
{
  removeEdge(QPair<QPair<QString, QString>, QString>(QPair<QString, QString>(source, target), name));
}

void Graph::removeEdge(const QPair<QPair<QString, QString>, QString>& key) {
  if(_edges.contains(key)) {
    agdelete(_graph, _edges[key]);
    _edges.remove(key);
  }
}

void Graph::applyLayout() {
  gvFreeLayout(_context, _graph);
  gvLayout(_context, _graph, "dot");
  gvRender(_context, _graph, "dot", nullptr);
}

void Graph::free()
{
  gvFreeLayout(_context, _graph);
}

QRectF Graph::boundingRect() const {
  qreal dpi=_agget(_graph, "dpi", "96.0").toDouble();
  return QRectF(GD_bb(_graph).LL.x*(dpi/DotDefaultDPI),
                GD_bb(_graph).LL.y*(dpi/DotDefaultDPI),
                GD_bb(_graph).UR.x*(dpi/DotDefaultDPI),
                GD_bb(_graph).UR.y*(dpi/DotDefaultDPI));
}

QList<Path> Graph::edges() const
{
  QList<Path> list;
  qreal dpi = _agget(_graph, "dpi", "96.0").toDouble();

  for(Agedge_t* edge : _edges){
    Path object;

    if ( (ED_spl(edge)->list != 0) &&
         (ED_spl(edge)->list->size % 3 == 1)) {
      qreal x, x2, y, y2;
      if (ED_spl(edge)->list->sflag) {
        x = ED_spl(edge)->list->sp.x*(dpi/DotDefaultDPI);
        y = (GD_bb(_graph).UR.y - ED_spl(edge)->list->sp.y)*(dpi/DotDefaultDPI);
        x2 = ED_spl(edge)->list->list[0].x*(dpi/DotDefaultDPI);
        y2 = (GD_bb(_graph).UR.y - ED_spl(edge)->list->list[0].y)*(dpi/DotDefaultDPI);
      } else {
        x = ED_spl(edge)->list->list[0].x*(dpi/DotDefaultDPI);
        y = (GD_bb(_graph).UR.y - ED_spl(edge)->list->list[0].y)*(dpi/DotDefaultDPI);
        x2 = ED_spl(edge)->list->list[1].x*(dpi/DotDefaultDPI);
        y2 = (GD_bb(_graph).UR.y - ED_spl(edge)->list->list[1].y)*(dpi/DotDefaultDPI);
      }

      object.points.moveTo(x, y);
      if (ED_spl(edge)->list->sflag)
        object.points.lineTo(x2, y2);

      object.setLabel(agnameof(edge));
      qreal xpos, ypos;
      xpos = ED_label(edge)->pos.x * (dpi/DotDefaultDPI);
      ypos = ED_label(edge)->pos.y * (dpi/DotDefaultDPI);

      object.setLabelPos(QPoint(xpos, ypos));

      for (int i = 1; i < ED_spl(edge)->list->size; i+=3)
        object.points.cubicTo(ED_spl(edge)->list->list[i].x*(dpi/DotDefaultDPI),
                            (GD_bb(_graph).UR.y - ED_spl(edge)->list->list[i].y)*(dpi/DotDefaultDPI),
                            ED_spl(edge)->list->list[i+1].x*(dpi/DotDefaultDPI),
                            (GD_bb(_graph).UR.y - ED_spl(edge)->list->list[i+1].y)*(dpi/DotDefaultDPI),
                            x2 = ED_spl(edge)->list->list[i+2].x*(dpi/DotDefaultDPI),
                            y2 = (GD_bb(_graph).UR.y - ED_spl(edge)->list->list[i+2].y)*(dpi/DotDefaultDPI));

      if (ED_spl(edge)->list->eflag) {
        object.points.lineTo(x = ED_spl(edge)->list->ep.x*(dpi/DotDefaultDPI),
                             y = (GD_bb(_graph).UR.y - ED_spl(edge)->list->ep.y)*(dpi/DotDefaultDPI));
      }

      object.points.addPolygon(createTriangle(15, atan((x2-x)/(y-y2)) + (x2 < x || y2 < y?0:M_PI), x, y));
     }

    list << object;
  }

  return list;
}

QPolygon Graph::createTriangle(qreal r, qreal phi, qreal x, qreal y) const {
  QVector<QPoint> triangle;
  triangle.append(QPoint(r*sin(phi+M_PI/6)+x,-r*cos(phi+M_PI/6)+y));
  triangle.append(QPoint(x, y));
  triangle.append(QPoint(r*sin(phi-M_PI/6)+x,-r*cos(phi-M_PI/6)+y));
  return QPolygon(triangle);
}

QList<Node> Graph::nodes() const
{
  QList<Node> list;
  qreal dpi = _agget(_graph, "dpi", "96.0").toDouble();

  for (Agnode_t* const node : _nodes) {
    Node object;
    char *lbl = agnameof(node);
    if (lbl)
      object.label = lbl;
    qreal x = ND_coord(node).x*(dpi/DotDefaultDPI);
    qreal y = ND_coord(node).y*(dpi/DotDefaultDPI);

    object.position = QPoint(x, y);
    object.height = ND_height(node)*dpi;
    object.width = ND_width(node)*dpi;

    x = ND_label(node)->pos.x * (dpi / DotDefaultDPI);
    y = ND_label(node)->pos.y * (dpi / DotDefaultDPI);

    object.setLabelPosition(QPoint(x, y));

    list << object;
  }

  return list;
}

Agraph_t *Graph::_agopen(QString name, Agdesc_t& kind) {
  return agopen(const_cast<char *>(qPrintable(name)), kind, 0);
}

QString Graph::_agget(void *object, QString attr, QString alt) {
  QString str=agget(object, const_cast<char *>(qPrintable(attr)));

  if(str==QString())
    return alt;
  else
    return str;
}

int Graph::_agset(void *object, QString attr, QString value) {
  return agsafeset(object, const_cast<char *>(qPrintable(attr)),
                   const_cast<char *>(qPrintable(value)),
                   const_cast<char *>(qPrintable(value)));
}

Agsym_t *Graph::_agattr(Agraph_t *graph, int kind, QString name, QString value)
{
  return agattr(graph, kind,
                const_cast<char *>(qPrintable(name)),
                const_cast<char *>(qPrintable(value)));
}

